import Vue from 'vue'
import Router from 'vue-router'
import InicioComponent from '@/components/inicio'
import LoginComponent from '@/components/login'
import SchoolsComponent from '@/components/schools'
import Courses from '@/components/courses'
import Asistencia from '@/components/asistencia'
import AsistenciaShow from '@/components/asistencia_show'
import ListaAsistencias from '@/components/lista_asistencias'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [{path: '/', component: InicioComponent},
    {path: '/login', component: LoginComponent, meta: {requiresGuest: true}},
    {path: '/escuelas', component: SchoolsComponent, meta: {requiresAuth: true}},
    {path: '/escuelas/:id/courses', component: Courses, meta: {requiresAuth: true}},
    {path: '/asistencia/:id/new', component: Asistencia, meta: {requiresAuth: true}},
    {path: '/asistencia/:id', component: AsistenciaShow, meta: {requiresAuth: true}},
    {path: '/lista/:id', component: ListaAsistencias, meta: {requiresAuth: true}}
  ]
})
