import md5 from 'js-md5'
function generateSecret (controller, action, Version, tokenUser, dateToday) {
  // console.log(controller + '-' + action + '-' + Version + '-' + tokenUser + '-' + dateToday)
  let stringKey = process.env.SECRET_APP + '-' + controller + '-' + action + '-' + Version + '-' + tokenUser + '-' + dateToday
  let mdKey = md5(stringKey)
  return mdKey
}

export default {
  getHeaders (withSession, controller, action, Version) {
    const today = new Date()
    let tokenUser = 'nosession'
    if (withSession === true && localStorage.getItem('_userP')) {
      tokenUser = JSON.parse(localStorage.getItem('_userP')).authentication_token
    }
    let numberMonth = today.getMonth() + 1
    if (numberMonth < 10) { numberMonth = '0' + numberMonth }
    let dayMonth = today.getDate()
    if (dayMonth < 10) { dayMonth = '0' + dayMonth }
    return {
      'Content-Type': 'application/json',
      'X-version': 'v1',
      'X-token': process.env.SECRET_APP,
      'X-secret': generateSecret(controller, action, Version, tokenUser, today.getFullYear() + '-' + numberMonth + '-' + dayMonth),
      'X-device-date': today.getFullYear() + '/' + numberMonth + '/' + dayMonth/* año/mes/dia */,
      'X-admin-token': tokenUser
    }
  }
}
