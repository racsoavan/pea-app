'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  PATH_API: '"https://pea-server.herokuapp.com"',
  SECRET_APP: '"24fa9d8431fb640345d32f9e0c8ec275"'
})
